<?php
header('Location: http://shop.elvatron.com/index.php/');
define("UPLOAD_DIR", "payments/");
 
// process file upload
if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_FILES["myFile"])) {
    
    $myFile = $_FILES["myFile"];
    
    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        // echo "<p>An error occurred.</p>";
    }

    // ensure a safe filename

    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

    // don't overwrite an existing file

    $i = 0;

    $parts = pathinfo($name);

    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }

    // preserve file from temporary directory

    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);

    if (!$success) {
        // echo "<p>Unable to save file.</p>";
    }

    $to      = 'andresalvivar@gmail.com';
	$subject = 'ElvaShop - Nuevo recibo de pago';
	$message = "# de orden: {$_POST['myCode']}\r\n# de transferencia: {$_POST['myNumber']}\r\nRecibo: http://shop.elvatron.com/payments/{$name}";
	$headers = 
		'From: elvashop@elvatron.com' . "\r\n" .
    	'Reply-To: elvashop@elvatron.com' . "\r\n" .
    	'X-Mailer: PHP/' . phpversion();

	mail($to, $subject, $message, $headers);

    // set proper permissions on the new file

    chmod(UPLOAD_DIR . $name, 0644);

    // echo "<p>Uploaded file saved as " . $name . ".</p>";

}

?>